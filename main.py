import os
import webapp2
import jinja2
import random
import string
import hashlib
import json
import logging
import time
from google.appengine.api import memcache
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

def make_salt():
    salt = ''
    for i in range(0, 5):
        salt += random.choice(string.letters)
    return salt

def password_hash(name, pw, salt = ''):
    if not salt:
        salt = make_salt()
    h = make_hash(name + pw + salt)
    return '%s|%s' % (h, salt)

def make_hash(word):
    return hashlib.sha256(word).hexdigest()

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)
    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)
    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

class MainPage(Handler):
    def get(self):
        entries = memcache.get('entries')
        secs = memcache.get('front-secs')
        if entries == None or secs == None:
            entries = db.GqlQuery("SELECT * FROM Entry ORDER BY dateCreated DESC")
            entries = list(entries)
            secs = time.time()
            memcache.set('entries', entries)
            memcache.set('front-secs', int(secs))
        now_secs = int(time.time())
        diff_secs = now_secs - secs
        self.render("front.html", entries = entries, secs = int(diff_secs))

class NewPost(Handler):
    def get(self):
        self.render("newpost.html")
    def post(self):
        title = self.request.get("subject")
        content = self.request.get("content")
        if title and content:
            entry = Entry(title = title, content = content)
            entry_key = entry.put()
            
            memcache.delete('entries')
            memcache.delete('front-secs')
            
            self.redirect("/" + str(entry_key.id()))
        else:
            error = "Both the title and content are needed."
            self.render("newpost.html", title = title, content = content, error = error)

class BlogPost(Handler):
    def get(self, entry_id):
        entry = memcache.get('entry-' + entry_id)
        secs = memcache.get('secs-' + entry_id)
        if entry == None or secs == None:
            entry = Entry.get_by_id({int(entry_id)})
            secs = int(time.time())
            memcache.set('entry-' + entry_id, entry)
            memcache.set('secs-' + entry_id, secs)
        diff_time = int(time.time()) - int(secs)
        self.render("front.html", entries = entry, secs = diff_time)

class SignUpHandler(Handler):
    NAME_VAR = "username"
    PASSWORD_VAR = "password"
    VERIFY_VAR = "verify"
    EMAIL_VAR = "email"
    ERROR_NAME = "Mandatory field."
    ERROR_PASSWORD = "Mandatory field."
    ERROR_VERIFY = "Passwords didn't match."
    def get(self):
        self.render("signup.html")
    def post(self):
        user_input = self.get_input()
        error_msgs = self.validate_input(user_input)
        if error_msgs:
            self.render(
                "signup.html",
                name = user_input[self.NAME_VAR],
                email = user_input[self.EMAIL_VAR],
                name_error = error_msgs[self.NAME_VAR],
                password_error = error_msgs[self.PASSWORD_VAR],
                verify_error = error_msgs[self.VERIFY_VAR])
        else:
            db_users = db.GqlQuery("SELECT * FROM User WHERE name = '%s'" % user_input[self.NAME_VAR])
            if db_users.count() == 0:
                hashed_pw = password_hash(user_input[self.NAME_VAR], user_input[self.PASSWORD_VAR])
                user = User(
                    name = user_input[self.NAME_VAR], 
                    password = hashed_pw, 
                    email = user_input[self.EMAIL_VAR])
                key = user.put()
                cookie_value = str(key.id()) + '|' + make_hash(user.name)
                self.response.headers.add_header('Set-Cookie', 'user=%s; Path=/' % cookie_value)
                self.redirect('/welcome')
            else:
                self.render(
                    'signup.html', 
                    name = user_input[self.NAME_VAR],
                    email = user_input[self.EMAIL_VAR],
                    name_error = 'User already registered.')
    def get_input(self):
        input_vars = {
            self.NAME_VAR : self.request.get(self.NAME_VAR),
            self.PASSWORD_VAR : self.request.get(self.PASSWORD_VAR),
            self.VERIFY_VAR : self.request.get(self.VERIFY_VAR),
            self.EMAIL_VAR : self.request.get(self.EMAIL_VAR)}
        return input_vars
    def validate_input(self, input_vars):
        error_msgs = {
            self.NAME_VAR : "",
            self.PASSWORD_VAR : "",
            self.VERIFY_VAR : ""}
        has_error = False
        if not input_vars[self.NAME_VAR]:
            has_error = True
            error_msgs[self.NAME_VAR] = self.ERROR_NAME
        if not input_vars[self.PASSWORD_VAR]:
            has_error = True
            error_msgs[self.PASSWORD_VAR] = self.ERROR_PASSWORD
        elif input_vars[self.PASSWORD_VAR] != input_vars[self.VERIFY_VAR]:
            has_error = True
            error_msgs[self.VERIFY_VAR] = self.ERROR_VERIFY
        if has_error:
            return error_msgs

class WelcomeHandler(Handler):
    def get(self):
        cookie = self.request.cookies.get('user')
        if cookie:
            user_id = cookie.split('|')[0]
            user = User.get_by_id(int(user_id))
            if make_hash(user.name) == cookie.split('|')[1]:
                self.render('welcome.html', name = user.name)
            else:
                self.redirect('/signup')
        else:
            self.redirect('/signup')

class LoginHandler(Handler):
    def get(self):
        self.render('login.html')
    def post(self):
        name = self.request.get('username')
        password = self.request.get('password')
        if not (name and password):
            self.render('login.html', name = name, error = 'Please enter username and password.')
        else:
            users = db.GqlQuery("SELECT * FROM User WHERE name = '%s'" % name)
            if users.count() == 1:
                user = users[0]
                user.password = password_hash(name, password)
                user.put()
                cookie_value = '%s|%s' % (str(user.key().id()), make_hash(user.name))
                self.response.headers.add_header('Set-Cookie', 'user=%s; Path=/' % cookie_value)
                self.redirect('/welcome')
            elif users.count() == 0:
                self.render('login.html', name = name, error = 'Invalid username or password.')
            else:
                self.write("This shouldn't happen.")
                

class LogoutHandler(Handler):
    def get(self):
        self.response.headers.add_header('Set-Cookie', 'user=; Path=/')
        self.redirect('/signup')


class JsonHandler(Handler):

    def parse(self, entries):
        entriesList = []
        for entry in entries:
            data = {
                'subject' : entry.title,
                'content' : entry.content,
                'dateCreated' : str(entry.dateCreated)}
            entriesList.append(data)
        return json.dumps(entriesList)

    def outputJson(self, jsonData):
        self.response.headers['Content-Type'] = 'application/json'
        self.write(jsonData)


class MainPageJson(JsonHandler):

    def get(self):
        entries = self.getAllBlogEntries()
        jsonData = self.parse(entries)
        self.outputJson(jsonData)

    def getAllBlogEntries(self):
        return Entry.all()


class PermalinkJson(JsonHandler):

    def get(self, entryId):
        entry = self.getBlogEntry(entryId)
        jsonData = self.parse(entry)
        self.outputJson(jsonData)

    def getBlogEntry(self, entryId):
        return Entry.get_by_id({int(entryId)})

class FlushHandler(Handler):
    
    def get(self):
        memcache.flush_all()
        self.redirect('/')

class Entry(db.Model):
    title = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    dateCreated = db.DateTimeProperty(auto_now_add = True)

class User(db.Model):
    name = db.StringProperty(required = True)
    password = db.StringProperty(required = True)
    email = db.StringProperty(required = False)

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/newpost', NewPost),
    ('/([0-9]+)', BlogPost),
    ('/signup', SignUpHandler),
    ('/welcome', WelcomeHandler),
    ('/login', LoginHandler),
    ('/logout', LogoutHandler),
    ('/.json', MainPageJson),
    ('/([0-9]+).json', PermalinkJson),
    ('/flush', FlushHandler)
], debug=True)
